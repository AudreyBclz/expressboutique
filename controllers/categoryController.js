const {Category} = require('../config/sequelize');

exports.getACategory = (req,res) =>{
    let id = req.params.id;
    try{
        Category.findByPk(id)
        .then(category =>{
            if(category){
                return res.status(200).send({message:`La catégorie n° ${id}`,data:category})
            }else{
                return res.status(400).send({message:`Il n'y a pas de catégorie n° ${id}`})
            }
        })
    }catch(error){
        res.status(500).send({message: error.message})
    }
    
}
   
exports.getAllCategories= (req,res) =>{
    try{
        Category.findAll()
        .then((cat =>{
            if(cat){
                return res.status(200).send({message:`Voici toutes les catégories:`,data:cat})
            }else{
                return res.status(400).send({message:`Il n'y a pas de catégorie`})
            }
        }))
    }catch(error){
        return res.status(500).send({message:error.message})
    }
}