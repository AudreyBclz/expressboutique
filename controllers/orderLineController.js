const {Orderline} = require('../config/sequelize');

exports.addOrderLine = (req,res) =>{
    try{
        Orderline.create({
            nameProduct:req.body.nameProduct,
            quantity:req.body.quantity,
            taille:req.body.taille,
            color:req.body.color,
            priceUnit:req.body.priceUnit,
            idOrder:req.body.idOrder
        })
        .then(orderline =>{
            return res.status(200).send({message:"La ligne de commande a été créé",data:orderline})
        })
    }catch(error){
        res.status(500).send({message:error.message})
    }
}

exports.findOrderLineByIdOrder = (req,res) =>{
    let idOrder = req.params.idOrder
    try{
        Orderline.findAll({
            where:{
                idOrder:idOrder
            }
        })
        .then(orders =>{
            if(orders){
             return res.status(200).send({message: `Voici les lignes de commandes de la commande n° ${idOrder}`,data:orders})   
            }else{
                return res.status(400).send({message: `Il n'y pas pas de ligne de commande pour l'id n° ${idOrder}`})
            }
        })
    }catch(error){
        res.status(500).send({message:error.message})
    }
}