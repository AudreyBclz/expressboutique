const {Order} = require('../config/sequelize');
const {v4} = require('uuid');

exports.addAOrder= (req,res) =>{
    try{
        Order.create({
            numCommande: v4(),
            date:new Date(),
            idUser:req.body.idUser
        })
        .then(order =>{
            return res.status(200).send({message:"La commande a bien été créée",data:order})
        })
    }catch(error){
        return res.status(500).send({message: error.message});
    }
}
exports.findAOrder = (req,res) =>{
    let id= req.params.id;
    try{
        Order.findByPk(id)
        .then(order =>{
            if(order){
                return res.status(200).send({message:`Voici la commande n° ${id}`,data:order})
            }else{
                return res.status(400).send({message:`La commande n° ${id} n'a pas été trouvée`})
            }
        })
    }catch(error){
        return res.status(500).send({message: error.message})
    }
}
exports.findOrdersByIdUser = (req,res) =>{
    let idUser = req.params.idUser;
    try{
        Order.findAll({
            where :{
                idUser : idUser
            }
        }).then(orders =>{
            if(orders){
                return res.status(200).send({message: `Voici les commandes de l'utilisateur n° ${idUser}`,data:orders})
            }else{
                return res.status(400).send({message :`L'utilisateur n° ${idUser} n'a pas de commande ou n'existe pas`})
            }
        })
    }catch(error){
        return res.status(500).send({message: error.message});
    }
}