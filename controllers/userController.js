const {User} = require("../config/sequelize");
const bcrypt = require("bcrypt");
const { ValidationError } = require("sequelize");

exports.inscription = (req, res) => {
  User.findOne({
    where: {
      email: req.body.email,
    },
  }).then((user) => {
    if (!user) {
      let { name, email, password, phone, sexe, adresse, city } = req.body;
      bcrypt.hash(password, 10).then((passhash) => {
        User.create({
          name: name,
          email: email,
          password: passhash,
          phone: phone,
          sexe: sexe,
          adresse: adresse,
          city: city,
        })
          .then((user) => {
            res.send({
              message: "L'Utilisateur a bien été enregistré",
              data: user,
            });
          })
          .catch((error) => {
            if (error instanceof ValidationError) {
              return res
                .status(400)
                .json({ message: error.message, data: error.errors });
            }
            res.status(500).send({ message: error.message });
          });
      });
    }
  });
};

exports.connexion = function(req,res){
    try{
        let{email,password}=req.body;

        User.findOne({
            where: {
                email:email
            },
        }).then((user) =>{
            if(!user){
                return res.status(400).send({message:"L'utilisateur n'a pas été trouvé"});
            }
            var passwordIsValid = bcrypt.compareSync(password,user.password);

            if(!passwordIsValid){
                return res.status(401).send({message:"Mot de passe incorrect"});
            }
            res.send({
                data: {
                    id:user.id,
                    name:user.name
                },
                status:200
            });
        });
    }catch(error){
        res.status(500).send({error:error.message});
    }
}

exports.findOneUser = (req,res) =>{
    let id = req.params.id;
    try{
        User.findOne({
            where :{
                id:id
            }
        }).then(user =>{
            if(user != undefined||null){
                res.send({
                    data:user,
                    status:200
                });
            }else{
                return res.status(401).send({message: "L'utilisateur n'existe pas"})
            }
        })
    }catch{
        return res.status(500).send({error: error.message})
    }
}
