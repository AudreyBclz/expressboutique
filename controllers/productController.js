const {Product} = require("../config/sequelize");

exports.getProducts =(res)=>{
    try{
        Product.findAll()
        .then(products =>{
            return res.status(200).send({message:"Voici la liste des produits.",data:products})
        })
    }catch(error){
        res.status(500).send({message:error.message})
    }
}
exports.getAProduct = (req,res) =>{
    let id=req.params.id;
    try{
        Product.findByPk(id)
        .then(product =>{
            if(product){
                return res.status(200).send({message: `Détails du produit n°${id}`,data:product})
            }else{
                return res.status(400).send({message:"L'id que vous avez entré n'existe pas"})
            }       
        })
    }catch(error){
        res.status(500).send({message:error.message})
    }
}

exports.getProductsByCat = (req,res) =>{
    idCat = req.params.idCat;
    try{
        Product.findAll({
            where: {
                idCategory:idCat
            }
        }).then(products =>{
            if(products){
                return res.status(200).send({message:`Voici les produits de la catégorie n°${idCat}`,data:products})
            }else{
                return res.status(400).send({message: "Il n'y a pas de produits"})
            }
        })
    }catch(error){
        res.status(500).send({message:error.message})
    }
}

exports.getFeaturedProd = (req,res) =>{
    try{
        Product.findAll({
            where:{
             feature:1   
            } 
        }).then(products =>{
            if(products){
                return res.status(200).send({message: "Voici les produits feature",data:products})
            }else{
                return res.status(400).send({message:"Il n'y a pas de produit feature"})
            }
        })
    }catch(error){
        return res.status(500).send({message:error.message})
    }
}

exports.getBestSellProd = (req,res) =>{
    try{
        Product.findAll({
            where:{
             bestSell:1   
            }
        }).then(products =>{
            if(products){
                return res.status(200).send({message: "Voici les meilleures ventes",data:products})
            }else{
                return res.status(400).send({message:"Il n'y a pas de produit dans les meilleures ventes"})
            }
        })
    }catch(error){
        return res.status(500).send({message:error.message})
    }
}