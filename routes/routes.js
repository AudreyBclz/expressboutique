const express = require("express");
const app = express();

const { inscription, connexion, findOneUser} = require('../controllers/userController');
const { getProducts, getAProduct, getProductsByCat, getFeaturedProd, getBestSellProd } = require('../controllers/productController');
const { getACategory, getAllCategories } = require('../controllers/categoryController');
const { addAOrder, findAOrder, findOrdersByIdUser } = require('../controllers/orderController');
const { addOrderLine, findOrderLineByIdOrder } = require('../controllers/orderLineController');
const checkUpMail = require('../middleware/verifyRegister');

const router = express.Router();

router.post('/inscription',checkUpMail,inscription);
router.post('/connexion',connexion);
router.post('/order',addAOrder);
router.get('/user/:id',findOneUser);
router.get('/order/:id',findAOrder);
router.get('/order/user/:idUser',findOrdersByIdUser);
router.post('/order/orderline',addOrderLine);
router.get('/order/orderline/:idOrder',findOrderLineByIdOrder);
router.get('/products',getProducts);
router.get('/products/find/featured',getFeaturedProd);
router.get('/products/find/bestsell',getBestSellProd)
router.get('/products/sortbycategorie/:idCat', getProductsByCat)
router.get('/products/:id',getAProduct);
router.get('/category/:id',getACategory);
router.get('/category',getAllCategories);

app.use('/api/boutique',router);
module.exports=router;
