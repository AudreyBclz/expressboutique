const {DataTypes, Sequelize} = require("sequelize");

const UserModel = require('../models/userModel');
const ProductModel = require('../models/productModel')
const CategoryModel = require('../models/categoryModel');
const OrderModel = require('../models/orderModel');
const OrderlineModel = require('../models/orderlineModel');

const sequelize = new Sequelize('boutique','root','',{
    host:"localhost",
    dialect:"mariadb",
    logging:false,
});

const User = UserModel(sequelize,DataTypes);
const Product = ProductModel(sequelize,DataTypes);
const Category = CategoryModel(sequelize,DataTypes); 
const Order = OrderModel(sequelize,DataTypes);
const Orderline = OrderlineModel(sequelize,DataTypes)

module.exports = {User,Product,Category,Order,Orderline};

