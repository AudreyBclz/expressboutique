module.exports= (sequelize,DataTypes) =>{
    return sequelize.define(
        'Order',
        {
            id:{
                type:DataTypes.INTEGER,
                primaryKey:true,
                autoIncrement:true,
            },
            numCommande:{
                type:DataTypes.STRING,
                allowNull:false,
            },
            date:{
                type:DataTypes.DATE,
                allowNull:false
            },
            idUser:{
                type:DataTypes.INTEGER,
                allowNull:false
            }

        },
        {
            createdAt:false,
            updatedAt:false
        }
    )
}