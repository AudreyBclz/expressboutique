module.exports= (sequelize,DataType) =>{
    return sequelize.define(
        'Category',
        {
            id:{
                type:DataType.INTEGER,
                primaryKey:true,
                autoIncrement:true
            },
            name:{
                type:DataType.STRING,
                allowNull:false
            },
            image:{
                type:DataType.STRING,
                allowNull:false
            }
            
        },
        {
            createdAt:false,
            updatedAt:false
        }
        
    )
}