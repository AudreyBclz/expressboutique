module.exports = (sequelize,DataTypes)=>{
    return sequelize.define(
        "User",
        {
            id:{
                type:DataTypes.INTEGER,
                primaryKey:true,
                autoIncrement:true,
            },
            name:{
                type:DataTypes.STRING,
                allowNull:false,
            },
            email:{
                type:DataTypes.STRING,
                allowNull:false,
                unique: {msg: "Cet email est déjà utilisé"},
                validate: {
                    is: { 
                        args:["^([a-z]*)@([a-z]{2,10})(.fr|.com)$",'i'],
                        msg:"Erreur dans le format du mail"
                    }
                }
            },
            password:{
                type:DataTypes.STRING,
                allowNull:false,
            },
            phone:{
                type:DataTypes.STRING,
                allowNull:false,
                validate: {
                    is:{
                        args:["^[0]{1}[1-9]{1}[0-9]{8}$",'i'],
                        msg:"Format invalide du numéro de téléphone"
                    }
                }
            },
            sexe:{
                type:DataTypes.STRING,
                allowNull:false,
            },
            adresse:{
                type:DataTypes.STRING,
                allowNull:false,
            },
            city:{
                type:DataTypes.STRING,
                allowNull:false,
                len:[2,30]
            },
            
        },
        {
            createdAt:false,
            updatedAt:false
        }
    )
}