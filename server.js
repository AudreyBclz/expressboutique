const express = require("express");
const bodyParser =require('body-parser');
const cors=require('cors');
const indexRouter = require('./routes/routes');
const sequelize=require('./config/sequelize');

const app = express();

var corsOptions ={
    origin:"http://localhost:8100"
}

app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(function(req,res,next){
    res.header(
        "Access-Control-Allow-Headers",
        "Authorization, Origin, Content-Type, Accept"
    );
    next();
});



app.use("/api/boutique",indexRouter);

app.listen(4000,() =>{
    console.log("listening on port 4000")
});