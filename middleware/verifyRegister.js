const {User}=require('../config/sequelize');
//Ne pas oublier de mettre les {} quand on a plusieurs tables

checkUpMail = (req,res,next) =>{
    User.findOne({
        where: {
            email:req.body.email
        }
    }).then((user) =>{
        if(user){
            return res.status(400).send({
                message:"L'email est déjà utilisée."
            });
        }else{
            next();
        }
    });
};

module.exports = checkUpMail;